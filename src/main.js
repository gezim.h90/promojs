import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import BootstrapVue from 'bootstrap-vue';
import VueApollo from 'vue-apollo';
import { sync } from 'vuex-router-sync';

import App from './App.vue';
import router from './router';
import store from '@/data/store';
import { apolloProvider } from '@/utils/graphql';
import '@/utils/prototypes';

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);
Vue.use(VueApollo);

sync(store, router);

Vue.prototype.$baseUrl = 'http://jsonplaceholder.typicode.com';

Vue.prototype.$handleError = (err) => {
  alert(err);
};

new Vue({
  router,
  store,
  apolloProvider,
  render(h) {
    return h(App);
  },
  created() {
    Vue.prototype.$state = this.$store.state;

    window.addEventListener('message', () => {
      if (process.env.NODE_ENV === 'development') {
        console.clear();
      }
    });
  },
}).$mount('#app');
