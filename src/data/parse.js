export const schemaTypes = [
  { type: 'String' },
  { type: 'Date' },
  { type: 'ACL' },
  { type: 'Boolean' },
  { type: 'Relation' },
  { type: 'Object' },
  { type: 'Number' },
  { type: 'File' },
  { type: 'Polygon' },
  { type: 'GeoPoint' },
  { type: 'Array' },
]

// These should not be editable
export const defaultSchemaTypes = [
  'objectId', 'createdAt', 'updatedAt', 'ACL'
]
