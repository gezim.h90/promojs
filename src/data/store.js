/* eslint-disable no-param-reassign */

import Vue from 'vue';
import Vuex from 'vuex';
import gql from 'graphql-tag';



Vue.use(Vuex);

export default new Vuex.Store({
  // Component based
  state: {
    SchemaTypeInputs: {
      fieldName: '',
    }
  },
  mutations: {
  },
  actions: {

  },
});
