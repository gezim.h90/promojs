// TODO: Get history of changes to the object

export default class BuildObject {
  constructor(obj) {
    this.obj = obj;
    this.original = { ...obj };
  }

  set(key, value) {
    if (!(key in this.obj)) {
      console.log('Key not in object');
      throw `Key ${key} not in initial object`;
    }
    this.obj[key] = value;
    
    return this;
  }

  replace(newobj) {
    const newKeys = this.newobj.keys(this.newobj);
    const objKeys = this.obj.keys(this.obj);

    if (!newKeys.equalValues(objKeys)) {
      throw "Keys don't match";
    }

    this.obj = newobj;
  }

  extend(obj) {
    return {
      ...this.obj, ...obj
    };
  }

  get(commit = false) {
    const constructed = { ...this.obj };
    if (commit) {
      this.commit();
    }
    return constructed;
  }

  commit() {
    this.obj = { ...this.original };
  }
}

export class ListOfObjects extends BuildObject {
}
