Array.prototype.equalValues = function (compareTo) {
  if (this.length !== compareTo.length) {
    return false;
  }

  for (let i = 0; i < this.length; i++) {
    if (!compareTo.includes(this[i])) {
      return false;
    }
  }

  return true;
};
