import ApolloClient from 'apollo-boost';
import VueApollo from 'vue-apollo';


const graphQLClient = new ApolloClient({
  uri: 'http://localhost:1337/graphql',
});

const apolloProvider = new VueApollo({
  defaultClient: graphQLClient,
});

export {
  graphQLClient, apolloProvider
}
