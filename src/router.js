import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import CreateModel from './views/CreateModel.vue';
import List from './views/List.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/edit/:schemaName',
      name: 'edit',
      component: Home,
    },
    {
      path: '/create-model',
      name: 'create_model',
      component: CreateModel,
    },
    {
      path: '/list',
      name: 'list',
      component: List,
    },
  ],
});
