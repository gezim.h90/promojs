module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/require-v-for-key': 'off',
    'no-alert': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-plusplus': 'off',
    'comma-dangle': 'off',
    'no-throw-literal': 'off',
    'no-restricted-syntax': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
