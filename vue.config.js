const CleanTerminalPlugin = require('clean-terminal-webpack-plugin');


module.export = {
  configureWebpack: {
    plugins: [
      new CleanTerminalPlugin(),
    ],
  },
}
